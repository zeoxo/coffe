// import Vue from 'nativescript-vue'
// import Vuex from 'vuex';
// import * as ApplicationSettings from "application-settings";
const Vue = require("nativescript-vue");
const Vuex = require("vuex");
const ApplicationSettings = require("application-settings");



Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        userInfo:{
            userID : "",
            userEmail: "",
            userName: "",
            userPassword:""
        }
    },
    mutations: {
        load(state) {
            if(ApplicationSettings.getString("store")) {
                this.replaceState(
                    Object.assign(state, JSON.parse(ApplicationSettings.getString("store")))
                );
            }
        },
        saveUserInfo(state, data) {
            // state.userInfo.userID = data.userUD;
            state.userInfo.userEmail = data.userEmail;
            state.userInfo.userPassword = data.userPassword;
            state.userInfo.userName = data.userName;
        }
    }
});

Vue.prototype.$store = store;

module.exports = store;
