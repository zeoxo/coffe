import firebase from "nativescript-plugin-firebase"
import Vue from 'nativescript-vue'
import login from './components/login'
import VueDevtools from 'nativescript-vue-devtools'
import store from "./store";

if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}
// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production')


new Vue({
  render: h => h('frame', [h(login)]),

  created() {
    firebase.init().then(
        instance => {
            console.log("firebase.init done");
             // configure a listener:
            var listener = {
                onAuthStateChanged: function(data) {
                // console.log(data.loggedIn ? "Logged in to firebase" : "Logged out from firebase");
                if (data.loggedIn) {
                    // console.log("User info", data.user);
                }
                },
                thisArg: this
            };

            // add the listener:
            firebase.addAuthStateListener(listener);
        },
        error => {
            console.log(`firebase.init error: ${error}`);
        }
    );
}
}).$start()
